⁰# Skateboard

## Introduction

A selection of wheels,  boards... 
Few additional comments and tricks.  

A longer board might increase the stability. It seems that bigger wheels increase the top speed, largely. 
For greater speed, >= 70-75 mm or higher might be good fit. However, the board should be adapted to allow turning safely (without the contact board/wheel).

"Most longboard have no kicktails."



## Deck Type

Flared Brick: This shape is great for riders looking for a short wheelbase and standing platform with ample wheel clearance. A perfect freeride set up for topmount lovers who like a small responsive board.

Double Kick Cutaway: Upturned nose and tail offer opportunities for freestyle tricks while the “cutway design” helps prevent wheel bite. These boards can run the gamut from cruiser designs to freestyle or freeride. Best for intermediate or advanced riders.

Double Kick Downhill: This shape is a hybrid between a downhill oriented top mount and a traditional skateboard. Lower wheel clearance than a flared brick but better concave, nose, and tail for adding some ollies and tricks to your standard raw run.

Single Kick Downhill: A lot like the double kick downhill without the weight or functionality of a nose. Perfect for those looking for a killer downhill or freeride setup but still need a good tail.

Directional Radial Speed Board: Radical concave and directional shape make a blank canvas for downhill riders. A classic, minimal design much loved by intermediate to expert riders.

Drop-Through Double Kick Cutaway: Lowered riding platform makes these boards easy to push for beginners or commuters. Kicktails offer opportunities for tricks or kick turns. Cutaway design helps reduce wheelbite. Drop through mounting decreases maneuverability but may feel more stable at moderate speeds for some riders.

Drop-Through Symmetrical Cutaway: Lowered platform is great for pushing. Cutaway design eliminates wheel bite. Drop-through mounting reduces maneuverability but may feel more stable at moderate speeds for some riders. Great board for first time riders.





![](medias/1693350899-deck-type-1-man.png)


## Lower boards 

What is the advantage of a board with a lower center of gravity? 


Pros of Drop Through Decks

Drop through decks are mainly used for cruiser style setups, simply because most of the benefits cater towards that type of riding. Since drop through boards aren't as structurally strong, and typically flexier than a topmount or drop deck style deck, taking them up to higher speeds isn't as preferred. Since drop through decks get you very low, they serve a similar purpose as drop decks in the sense that they are much easier to push with in comparison to a topmount. Usually drop through decks are a little flexier than most, like the Loaded Dervish and the DB Coreflex, which are both awesome decks for low speed cruising, carving, and dancing. Stiffer downhill style drop through deck like the Landyachtz 9 to 5 or the Blackdog Belial serve a similar purpose to stiffer drop decks. They are very easy to slide considering that the center of gravity is much lower as well as it makes them much more stable.

Cons of Drop through Decks

Like stated above, drop through decks are not the strongest because all of your weight is distributed mainly on the 8 bolts holding the trucks to the board. This is why most drop through decks are flexy and suited more towards carving and cruising. This doesn't mean they are bad boards, they just cater to a different discipline of longboarding. Beyond the structural integrity, drop through boards are very drifty. For freeride, it makes initiating slides and sitting over them easier, but if you need any grip for corners or any other technical maneuvering, drop throughs are a losing idea. Therefore, downhilling with a drop deck will not be a good idea if you have any technical riding in mind. Along with the driftiness, the amount of truck response from drop through is the least out of all mounting styles. Because you are mounting the trucks physically through the board, you are getting nowhere near as much leverage due to the natural height and your foot's increased distance from the trucks. So, if you need any technical performance, such as taking corners or maintaining grip, maybe stay away from a drop through deck! 

DROP CAT LONGBOARD - Drop Through with Rocker

https://duckduckgo.com/?q=drop-through+double+kick+cutaway&t=h_&iar=videos&iax=videos&ia=videos&iai=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DxEMHe2FkkGI


## Wheels 

####  Type #1 - 70mm 

![](content/wheels/1.orangatang-4president-70mm-longboard-wheels-4-pack-p4.jpg)

![](content/wheels/20210919114400F1-IMG_1347.jpg)

####  Type #2  - 75mm 

Some Wheels  " 75mm, 56mm, contact 80a orangtang, In heat"




![](content/wheels/20210919115002F1-2.inheat-ORANGATANG_INHEATS_75MM_80A.png)




#### Larger wheels - 85mm 


![](https://gitlab.com/openbsd98324/skateboard/-/raw/main/content/wheels/1658742736-1-Orangatang-Caguama-85mm-80A-wheels.png)




## Selection Of Boards

### First One -- Multi-purpose, excellent for all types of fields: 

With larger wheels (as follows), including a Kick Tail.

![](content/longboard/35875905-excess-123887-0-Globe-TheAllTime35875905cm.jpg)

![](content/wheels/1.orangatang-4president-70mm-longboard-wheels-4-pack-p4.jpg)



### Second One: Excellent as "Starter" for skateparks / halls

Good Starter - Skateparks, Good for training, small wheels, Half-pipe... 

(Very small wheels, so take care of little rocks!)

 
![](content/skateboard/santacruz/1641589716-1-santacruz.png)




### Third One:  Ultimate Board with larger Wheels, ORANGATANG_INHEATS

Speed in Summer Time... ideal for down hills.

Drop type. 

Width is larger, stable wheels, so it is more stable and very good to drive faster and for hills. 

The good to excellent "Ferrari" ... quite fast with larger wheels, faster than with the 70 mm wheels.
![](https://gitlab.com/openbsd98324/skating/-/raw/main/content/longboard-drop/1632430473-arbor-solstice-37-orig-v1.png)


![](https://gitlab.com/openbsd98324/skateboard/-/raw/main/content/longboard/1658743458-1-Axis-37-Flagship-Deck.png)



![](content/wheels/20210919115002F1-2.inheat-ORANGATANG_INHEATS_75MM_80A.png)



*Description*
````
Axis 37 Solstice
Regular price €239,99
PRODUCT OPTIONS
L: 37.00" | W: 8.50" | WB: 27.75"

Premium Palisander Wood Topsheet

A snowboard-inspired drop-through for easy around town cruising, relaxed commutes, and mellow downhill.

Artist: Hilary Jane

A PORTION OF ALL SOLSTICE COLLECTION SALES BENEFITTING BOARDING FOR BREAST CANCER

MATERIALS & COMPONENTS
DECK
7 Ply Sustainable Canadian Maple & Palisander Wood Topsheet
Wood material comes from sustainable sources of supply
Wood by-product is reclaimed for use in other products
TRUCKS
Paris Reverse 50° 180mm
WHEELS
Arbor Easyrider Series - Outlook
Designed for carving and cruising
Larger shape increases rolling speed
Size: 69mm // Durometer: 78A
Contact Patch: 44.0mm // Offset: 0.0mm
Momentum Core: Dampens bumpy rides
Venice Formula: Soft and grippy
OTHER COMPONENTS
ABEC 7 Bearings with Spacers
Recycled Glass Re-Grit
````






### 4th, For Air and Jumps, light, white wheels, and faster + slightly larger wheels than the SantaCruz

Seems like better bearings than the SantaCruz.
Better multi-purpose board, quite light.



![](content/skateboard/birdhouse/1641245527-1-birdhouse-triple-stack-8-79e-v1.png)



### 5th Icarus

Default: 

Icarus flex-1, with orangatang 80mm 80a Kegel (orange color)


Slightly shorter, more flexible.

Lighter, made of cast titanium (?? eventually, it depends which model)



![](content/professional/Icarus/Icarus+38+4+Flex+2+Complete__1_.jpg)

 


Icarus default vs Solstice with 75mm in heat  

![](medias/20231008_202344.jpg)


### Solstice with new 17 Super Reds Bearings + 85 mm 80A for highest speed! 
 
 L: 37.00" | W: 8.50" | WB: 27.75"

Premium Palisander Wood Topsheet

A snowboard-inspired *drop-through* for easy around town cruising, relaxed commutes, and mellow downhill.

*Description*
````
Axis 37 Solstice
Regular price €239,99
PRODUCT OPTIONS
L: 37.00" | W: 8.50" | WB: 27.75"
````

bearings + new 85mm  80a 

![](content/bearing/1658765609-1-reeds-bones-super-superior-grade-steel-abec-skate-rated-39-95E-17Super-reds-bearings-uni-356995-size-uni-0002-v1.png)


![](https://gitlab.com/openbsd98324/skateboard/-/raw/main/1658778573-screenshot.png)


![](https://gitlab.com/openbsd98324/skateboard/-/raw/main/medias/1658778836-screenshot.png)





### Wheels: Downhill versus city

123... 
orangatang-4president-70mm *vs* 
ORANGATANG_INHEATS_75MM_80A.png *vs* 
Orangatang-Caguama-85mm-80A

The orangatang 4president are okay for multi purpose skating, however for the city, to keep top speed it is bit harder. 
However the Globe skateboard turns very well, ideally suited for jumps and pumptracks, even skate bowl. 
I tried almost the dropin and seems good too, as well, with 75mm. 

With in-heat 75mm, it is better to go for city for Solstice for higher speed and keeping that speed for long distance such as 10-20 km. 
Depending on the ground type. 
It is a very good compromise, excellent, fast, well suited for many things.

However, wanna race, faster? If you try to follow bikes, get really highest speed, then go for 85mm ! It is ideally to get top speed easily and to maintain it for longer distance. So, as fast as a bike, without much efforts. I took bearing reeds red, with higher steel grade, and they are really good. Better than the cheaper reeds, much difference can be obversed.
Despite the excellent top speed, it is more stable, in fact it is fine, but for downhill it can be a larger trouble. 
"Sticky" means that it is stable, but to get downhill and slide, it is much easier with the smallest like the president 70mm or 75mm (see globe). The globe skateboard does better (easily) sliding, much easier, which could be even too "much sliding" at some points. 

*multi-purpose*
The Globle skateboard is excellent for pumptrack and it rocks as well for jumps and tricks on skateparks, and bowl is too possible, QUick tail provided! You can dropin with it, bit heavier than a regular skateboard for skateparks. 




### Slow Down during Downhill




The famous question: Can we brake?

** Hand and Gloves

** sliding

** Wait and see :) 

** Grass on right side 


... any tricks that can do the job 

**Trick for longboard Solstice :  working on Solstice very Well **

Of course, there are several ways actually.  
When it goes really too fast in downhills, and it seems harder to keep it up, then, "the left back foot" on the back wheel (on the top) will allow to slow down to the comfortable speed. This is very easy, and it can really help in some eventual situation of potential danger or arriving objects in other direction.  
Drawback. Well, there is one or actually too. Your VANS shoes are solid but their are going to be damaged easily and quite fast. Being flat. 
Another, the wheels as well ! The wheels about 1cm from the side will be not there, about few mm, which affects on the performances. 
So, yeah, helpful, but do not overuse it. 



#### Overview  and table

![](content/overview/1632173140-screenshot.png)
 
```math
 ( 70 ) /  ( 53 )^3 * 77 * 10000
```


## Longboard

For higher speed, larger wheels are a good idea.

Example:
![](https://gitlab.com/openbsd98324/skateboard/-/raw/main/content/wheels/1658742736-1-Orangatang-Caguama-85mm-80A-wheels.png)


# 85.mm

LONGBOARD WHEEL SPECS

Diameter: 

 85mmWidth: 

 58.5mmContact Patch: 

 56mmStyle: 

 Rounded lips with surface skinBearing Seat: 

 OffsetFormula: 

 Happy ThaneCore: high-strength, high-stiffness, heat-resistant urethaneWeight (per wheel): 9.6 oz / 270 gDurometers: 

  80a

# Icarus

Riding Style: Carving

Boardbreite: 8.6 inchLänge (inch): 38.4 inchBreite: 21.8 cm

Länge (cm): 97.5 cm

Achsmontage: Drop-Through

Achsstand: 28.25 inch

Hängerbreite: 7 inch
Rollen Ø: 80 mm

Rollen: Kegel

ABEC-Wert: 7





#### Tipps.

1.) Wheels
wheels with specification " 75mm, 56mm, contact 80a orangtang In-heat" are excellent for high speed.
It is great for long distance on flat roads, long run, and also for the downhills.
It keeps speed over longer time, and it offers a way to travel in the city with outstanding speed.

"Larger wheels, higher top speed easily"

2.) Longer board

The longer board will increase the stability greater. 
However, too long might be too heavy.

Solstice (see above) is very stable and easy to drive.

*Depending on skater height. 

3.) Get faster, get larger wheel diameter 

orangatang with >= 85 mm and higher is great for getting faster with less efforts.



